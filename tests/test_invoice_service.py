from decimal import Decimal

import pytest
from django.contrib.auth.models import User
from sequences.models import Sequence

from agile_accounting.models import (
    ExchangeRate,
    Invoice,
    InvoiceBillingAddress,
    InvoiceDeliveryAddress,
    InvoicingEntity,
    Proforma,
)
from agile_accounting.services import Contact, InvoiceServiceException, InvoiceService, ProformaService
from tests.conftest import INITIAL_SEQUENCE_NUMBERS


@pytest.mark.django_db
def test_register_partial_payment(invoice: Invoice):
    invoicer = InvoiceService(invoice)
    invoicer.register_payment(invoice.total_gross() / 2, "OP 1")

    assert invoicer.invoice.rest_payment() == invoice.total_gross() / 2


@pytest.mark.django_db
def test_register_full_payment(invoice: Invoice):
    invoicer = InvoiceService(invoice)
    invoicer.register_payment(invoice.total_gross(), "OP 1")

    assert invoicer.invoice.rest_payment() == 0
    assert invoicer.invoice.paid is True


@pytest.mark.django_db
def test_generation_of_invoice_from_proforma(
    invoicer: InvoiceService,
    proforma: Proforma,
    exchange_rate: ExchangeRate,
):
    invoicer.generate_from_proforma(proforma.pk)
    invoice = Invoice.objects.latest("id")

    assert invoice.proformas.get(pk=proforma.pk) == proforma


@pytest.mark.django_db
def test_generation_of_invoice_from_proforma_with_billing(
    invoicer: InvoiceService,
    proforma: Proforma,
    exchange_rate: ExchangeRate,
    contact: Contact,
):
    contact.name = "Test name"
    invoicer.generate_from_proforma(proforma.pk, billing_address=contact)
    invoice = Invoice.objects.latest("id")

    assert invoice.billing_address.name == contact.name


@pytest.mark.django_db
def test_generation_of_invoice_from_proforma_with_delivery(
    invoicer: InvoiceService,
    proforma: Proforma,
    exchange_rate: ExchangeRate,
    contact: Contact,
):
    contact.name = "Test name"
    invoicer.generate_from_proforma(proforma.pk, delivery_address=contact)
    invoice = Invoice.objects.latest("id")

    assert invoice.delivery_address.name == contact.name


@pytest.mark.django_db
def test_generation_of_invoice_from_proforma_with_payment_type(
    invoicer: InvoiceService,
    proforma: Proforma,
    exchange_rate: ExchangeRate,
):
    invoicer.generate_from_proforma(proforma.pk, payment="op")
    invoice = Invoice.objects.latest("id")

    assert invoice.payment_type.code == "op"


@pytest.mark.django_db
def test_generation_of_invoice_from_proforma_with_exchange_date(
    invoicer: InvoiceService,
    proforma: Proforma,
    exchange_rate: ExchangeRate,
):
    exchange_rate.exchange_date = "2021-01-23"
    exchange_rate.save()

    invoicer.generate_from_proforma(proforma.pk, exchange_date="2021-01-23")
    invoice = Invoice.objects.latest("id")

    assert str(invoice.exchange_date) == "2021-01-23"


@pytest.mark.django_db
def test_generation_of_invoice_from_proforma_with_delivery_type(
    invoicer: InvoiceService,
    proforma: Proforma,
    exchange_rate: ExchangeRate,
):
    invoicer.generate_from_proforma(proforma.pk, delivery="courier")
    invoice = Invoice.objects.latest("id")

    assert invoice.delivery_type.code == "courier"


@pytest.mark.django_db
def test_invalid_generation_of_invoice_from_multiple_proformas(
    invoicer: InvoiceService,
    proforma: Proforma,
):

    with pytest.raises(InvoiceServiceException):
        invoicer.generate_from_proformas(proforma.pk)


@pytest.mark.django_db
def test_generation_of_invoice_from_multiple_proformas(
    contact: Contact,
    exchange_rate: ExchangeRate,
    invoicer: InvoiceService,
    invoicing_entity: InvoicingEntity,
    proforma: Proforma,
):
    proformer = ProformaService()
    proformer.set_invoicing_entity(invoicing_entity)
    proformer.set_service_type("sales")
    proformer.set_contact("billing", contact)
    proformer.set_contact("delivery", contact)
    proformer.add_item(
        unit_price=12.00,
        vat_percent=19.00,
        quantity=2,
        measure_unit="b",
        description="Test item #1",
    )
    proformer.add_item(
        unit_price=24.00,
        vat_percent=0.00,
        quantity=1,
        measure_unit="b",
        description="Test item #2",
    )
    proformer.save_invoice()
    proformas = ",".join([str(proforma.pk), str(proformer.invoice.pk)])

    invoicer.generate_from_proformas(proformas)
    invoice = Invoice.objects.latest("id")

    assert invoice.proformas.count() == 2


@pytest.mark.django_db
def test_invoice_storno(invoice: Invoice, exchange_rate: ExchangeRate):
    invoicer = InvoiceService(invoice)
    storno = invoicer.storno(invoice.pk)

    assert storno.storno_id == invoice.pk
    assert invoice.storno_id == storno.pk
    assert invoice.total_netto() == storno.total_netto() * -1
    assert invoice.total_vat() == storno.total_vat() * -1
    assert invoice.total_gross() == storno.total_gross() * -1


"""
Generic tests inherited from InvoiceServiceGeneric
"""


@pytest.mark.django_db
def test_invoice_invalid_reconstitution():
    with pytest.raises(InvoiceServiceException):
        InvoiceService(9999999)


@pytest.mark.django_db
def test_correct_reconstitution(invoice: Invoice):
    invoicer = InvoiceService(invoice)

    assert invoicer.invoice == invoice
    assert len(invoicer.invoice_items) == invoice.items.count()
    assert invoicer.billing == invoice.billing_address
    assert invoicer.delivery == invoice.delivery_address


@pytest.mark.django_db
def test_missing_billing(invoicer: InvoiceService):
    with pytest.raises(InvoiceServiceException) as e:
        invoicer.save_invoice()
        assert e == "Must provide a billing contact"


@pytest.mark.django_db
def test_invalid_billing(invoicer: InvoiceService, contact: Contact):
    with pytest.raises(InvoiceServiceException) as e:
        invoicer.set_contact("unknown", contact)
        assert e == "Target must be one of billing|delivery"


@pytest.mark.django_db
def test_missing_invoicing_entity(generic_invoicer: InvoiceService, contact: Contact):
    generic_invoicer.set_contact("billing", contact)

    with pytest.raises(InvoiceServiceException) as e:
        generic_invoicer.save_invoice()
        assert e == "Must provide a billing contact"


@pytest.mark.django_db
def test_invalid_invoicing_entity(generic_invoicer: InvoiceService):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_invoicing_entity(123123123123)
        generic_invoicer.set_invoicing_entity("wrong code")


@pytest.mark.django_db
def test_invalid_currency(generic_invoicer: InvoiceService):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_currency("wrong currency")


@pytest.mark.django_db
def test_invalid_delivery_type(generic_invoicer: InvoiceService):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_delivery_type("wrong delivery type")


@pytest.mark.django_db
def test_missing_service_type(generic_invoicer: InvoiceService, contact: Contact, invoicing_entity: InvoicingEntity):
    generic_invoicer.set_contact("billing", contact)
    generic_invoicer.set_invoicing_entity(invoicing_entity)

    with pytest.raises(InvoiceServiceException):
        generic_invoicer.save_invoice()


@pytest.mark.django_db
def test_invalid_service_type(generic_invoicer: InvoiceService):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_service_type("wrong service type")


@pytest.mark.django_db
def test_invalid_payment_type(generic_invoicer: InvoiceService):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_payment_type("wrong payment type")


@pytest.mark.django_db
def test_invalid_unit_price(generic_invoicer: InvoiceService):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.add_item("c", 19.00, 2, "b", "test")


@pytest.mark.django_db
def test_invalid_vat_rate(generic_invoicer: InvoiceService):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.add_item(12, 100.00, 2, "b", "test")


@pytest.mark.django_db
def test_exception_when_saving_with_no_items(invoice: Invoice):
    generic_invoicer = InvoiceService(invoice)
    generic_invoicer.drop_items()

    with pytest.raises(InvoiceServiceException):
        generic_invoicer.save_invoice()


@pytest.mark.django_db
def test_missing_exchange_rate(generic_invoicer: InvoiceService):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_exchange_rate("1990-01-01", currency_code="EUR")


@pytest.mark.django_db
def test_correct_exchange_rate_set(generic_invoicer: InvoiceService, exchange_rate: ExchangeRate):
    generic_invoicer.set_exchange_rate(
        exchange_date=exchange_rate.exchange_date,
        currency_code=exchange_rate.currency_code,
    )

    assert exchange_rate.exchange_rate == generic_invoicer.invoice.exchange_rate


@pytest.mark.django_db
def test_if_invoice_correctly_saved(invoice: Invoice):
    generic_invoicer = InvoiceService(invoice)

    # assert that contacts have been properly set
    assert invoice.billing_address.id == InvoiceBillingAddress.objects.latest("id").id
    assert generic_invoicer.billing.id == InvoiceBillingAddress.objects.latest("id").id
    assert invoice.delivery_address.id == InvoiceDeliveryAddress.objects.latest("id").id
    assert generic_invoicer.delivery.id == InvoiceDeliveryAddress.objects.latest("id").id

    # invoice should be unpaid when first created
    assert invoice.paid is False

    # check for proper incrementation of the sequence
    seq = Sequence.objects.get(name="test_invoice")
    assert int(INITIAL_SEQUENCE_NUMBERS[0][1]) == seq.last - 1

    # check if the invoice has 2 items as according to test conf
    assert invoice.items.count() == 2

    # check if total vat amount was properly calculated
    total_vat = round(Decimal(48) * Decimal(0.19), 2)
    assert invoice.total_vat() == total_vat

    # check if netto amount was properly calculated
    total_nettto = Decimal(96)
    assert invoice.total_netto() == total_nettto

    # check if gross amount was properly calculated
    total_gross = total_nettto + total_vat
    assert invoice.total_gross() == total_gross


@pytest.mark.django_db
def test_recalcualtion_of_totals_when_adding_item(invoice: Invoice):
    generic_invoicer = InvoiceService(invoice)
    old_total_vat = generic_invoicer.invoice.total_vat()
    old_total_netto = generic_invoicer.invoice.total_netto()

    # check if totals are properly recalculated when adding a new item
    generic_invoicer.add_item(20, 19.00, 1, "b", "Test item #3")
    generic_invoicer.save_invoice()

    invoice = generic_invoicer.invoice

    assert len(generic_invoicer.invoice_items) == 3
    assert invoice.items.count() == 3

    # check new total_vat
    new_total_vat = old_total_vat + round(Decimal(20) * Decimal(0.19), 2)
    assert invoice.total_vat() == new_total_vat

    # check new total_netto
    new_total_netto = old_total_netto + Decimal(20)
    assert invoice.total_netto() == new_total_netto

    # check new total_gross
    new_total_gross = new_total_netto + new_total_vat
    assert invoice.total_gross() == new_total_gross


@pytest.mark.django_db
def test_mark_as_paid(invoice: Invoice):
    generic_invoicer = InvoiceService(invoice)
    generic_invoicer.mark_paid()

    assert generic_invoicer.invoice.paid is True


@pytest.mark.django_db
def test_no_updates_if_paid(invoice: Invoice):
    generic_invoicer = InvoiceService(invoice)
    generic_invoicer.mark_paid()

    with pytest.raises(InvoiceServiceException):
        generic_invoicer.add_item(20, 19.00, 1, "b", "test")


@pytest.mark.django_db
def test_add_comment(invoice: Invoice, user: User):
    generic_invoicer = InvoiceService(invoice)
    generic_invoicer.add_user_comment(user.pk, "test comment")

    comments = invoice.comment.all()

    assert len(comments) == 1
    assert comments[0].text == "test comment"
