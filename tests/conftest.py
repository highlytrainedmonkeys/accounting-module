from datetime import date
from decimal import Decimal

import pytest
from ag_core.models import CountryCode, EntityType
from django.contrib.auth.models import User
from sequences.models import Sequence

from agile_accounting.models import ExchangeRate, Invoice, InvoicingEntity, Proforma
from agile_accounting.services import (
    Contact,
    ContactTypes,
    InvoiceService,
    InvoiceServiceGeneric,
    ProformaService,
)

INITIAL_SEQUENCE_NUMBERS = [("invoice", "45000"), ("quote", "25000")]


@pytest.fixture
def contact() -> Contact:
    contact = Contact()
    contact.type = ContactTypes.COMPANY.value
    contact.name = "Some Company"
    contact.email = "alex@agilegeeks.ro"
    contact.id1 = "RO1232123"
    contact.id2 = "J40/283773/2018"
    contact.phone = "+40722565194"
    contact.city = "Bucuresti"
    contact.country_code = "RO"
    contact.street1 = "Maresal Averescu Nr.8-10"
    contact.postal_code = "76293"
    contact.bank_account = "BRDEVR8675867235476534"
    contact.bank_name = "BRD Societe Generale"

    return contact


@pytest.fixture
def sequences() -> dict:
    sequences = {}

    for s in INITIAL_SEQUENCE_NUMBERS:
        seq = Sequence(name="test_" + s[0], last=s[1])
        seq.save()
        sequences[s[0]] = seq

    return sequences


@pytest.fixture
def invoicing_entity(sequences: dict) -> InvoicingEntity:
    invoicing_entity = InvoicingEntity()
    invoicing_entity.name = "Test Company SRL"
    invoicing_entity.code = "test"
    invoicing_entity.invoice_series = "1"
    invoicing_entity.invoice_sequence = sequences["invoice"]
    invoicing_entity.quote_sequence = sequences["quote"]
    invoicing_entity.id1 = "RO27852503"
    invoicing_entity.id2 = "J40/110229/1999"
    invoicing_entity.type = EntityType.objects.get(code="c")
    invoicing_entity.bank_name = "BRD Victoriei"
    invoicing_entity.bank_account = "RO66 BRDE 2312 V006 8117 4160"
    invoicing_entity.country_code = CountryCode.objects.get(code="RO")
    invoicing_entity.city = "București"
    invoicing_entity.postal_code = "123123"
    invoicing_entity.email = "office@agilegeeks.ro"
    invoicing_entity.street1 = "B-dul Nicolae Popescu, nr. 8-10, Sector 1"
    invoicing_entity.phone = "+40214057200"

    invoicing_entity.save()

    return invoicing_entity


@pytest.fixture
def proforma(invoicing_entity: InvoicingEntity, contact: Contact) -> Proforma:
    proforma = ProformaService()
    proforma.set_invoicing_entity(invoicing_entity)
    proforma.set_service_type("sales")
    proforma.set_contact("billing", contact)
    proforma.set_contact("delivery", contact)
    proforma.add_item(
        unit_price=12.00,
        vat_percent=19.00,
        quantity=4,
        measure_unit="b",
        description="Test item #1",
    )
    proforma.add_item(
        unit_price=24.00,
        vat_percent=0.00,
        quantity=2,
        measure_unit="b",
        description="Test item #2",
    )
    proforma.save_invoice()

    return proforma.invoice


@pytest.fixture
def invoice(invoicing_entity: InvoicingEntity, contact: Contact) -> Invoice:
    invoicer = InvoiceService()
    invoicer.set_invoicing_entity(invoicing_entity)
    invoicer.set_service_type("sales")
    invoicer.set_payment_type("online")
    invoicer.add_item(
        unit_price=12.00,
        vat_percent=19.00,
        quantity=4,
        measure_unit="b",
        description="Test item #1",
    )
    invoicer.add_item(
        unit_price=24.00,
        vat_percent=0.00,
        quantity=2,
        measure_unit="b",
        description="Test item #2",
    )
    invoicer.set_contact("billing", contact)
    invoicer.set_contact("delivery", contact)
    invoicer.save_invoice()

    return invoicer.invoice


@pytest.fixture
def generic_invoicer() -> InvoiceServiceGeneric:
    return InvoiceServiceGeneric()


@pytest.fixture
def invoicer() -> InvoiceService:
    return InvoiceService()


@pytest.fixture
def proformer() -> ProformaService:
    return ProformaService()


@pytest.fixture
def exchange_rate() -> ExchangeRate:
    ex = ExchangeRate(
        currency_code="EUR",
        exchange_date=date.today(),
        publishing_date=date.today(),
        exchange_rate=Decimal("4.1234"),
    )
    ex.save()

    return ex


@pytest.fixture
def user() -> User:
    user = User.objects.create_user("alex", "alex@agilegeeks.ro", "johnpassword")
    user.save()

    return User.objects.last()
