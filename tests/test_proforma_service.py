from decimal import Decimal

import pytest
from django.contrib.auth.models import User
from sequences.models import Sequence

from agile_accounting.models import (
    ExchangeRate,
    Proforma,
    ProformaBillingAddress,
    ProformaDeliveryAddress,
    InvoicingEntity,
)
from agile_accounting.services import Contact, InvoiceServiceException, ProformaService
from tests.conftest import INITIAL_SEQUENCE_NUMBERS


"""
Generic tests inherited from InvoiceServiceGeneric
"""


@pytest.mark.django_db
def test_invoice_invalid_reconstitution():
    with pytest.raises(InvoiceServiceException):
        ProformaService(9999999)


@pytest.mark.django_db
def test_correct_reconstitution(proforma: Proforma):
    proformer = ProformaService(proforma)

    assert proformer.invoice == proforma
    assert len(proformer.invoice_items) == proforma.proforma_items.count()
    assert proformer.billing == proforma.proforma_billing_address
    assert proformer.delivery == proforma.proforma_delivery_address


@pytest.mark.django_db
def test_missing_billing(proformer: ProformaService):
    with pytest.raises(InvoiceServiceException) as e:
        proformer.save_invoice()
        assert e == "Must provide a billing contact"


@pytest.mark.django_db
def test_invalid_billing(proformer: ProformaService, contact: Contact):
    with pytest.raises(InvoiceServiceException) as e:
        proformer.set_contact("unknown", contact)
        assert e == "Target must be one of billing|delivery"


@pytest.mark.django_db
def test_missing_invoicing_entity(proformer: ProformaService, contact: Contact):
    proformer.set_contact("billing", contact)

    with pytest.raises(InvoiceServiceException) as e:
        proformer.save_invoice()
        assert e == "Must provide a billing contact"


@pytest.mark.django_db
def test_invalid_invoicing_entity(proformer: ProformaService):
    with pytest.raises(InvoiceServiceException):
        proformer.set_invoicing_entity(123123123123)
        proformer.set_invoicing_entity("wrong code")


@pytest.mark.django_db
def test_invalid_currency(proformer: ProformaService):
    with pytest.raises(InvoiceServiceException):
        proformer.set_currency("wrong currency")


@pytest.mark.django_db
def test_invalid_delivery_type(proformer: ProformaService):
    with pytest.raises(InvoiceServiceException):
        proformer.set_delivery_type("wrong delivery type")


@pytest.mark.django_db
def test_missing_service_type(proformer: ProformaService, contact: Contact, invoicing_entity: InvoicingEntity):
    proformer.set_contact("billing", contact)
    proformer.set_invoicing_entity(invoicing_entity)

    with pytest.raises(InvoiceServiceException):
        proformer.save_invoice()


@pytest.mark.django_db
def test_invalid_service_type(proformer: ProformaService):
    with pytest.raises(InvoiceServiceException):
        proformer.set_service_type("wrong service type")


@pytest.mark.django_db
def test_invalid_payment_type(proformer: ProformaService):
    with pytest.raises(InvoiceServiceException):
        proformer.set_payment_type("wrong payment type")


@pytest.mark.django_db
def test_invalid_unit_price(proformer: ProformaService):
    with pytest.raises(InvoiceServiceException):
        proformer.add_item("c", 19.00, 2, "b", "test")


@pytest.mark.django_db
def test_invalid_vat_rate(proformer: ProformaService):
    with pytest.raises(InvoiceServiceException):
        proformer.add_item(12, 100.00, 2, "b", "test")


@pytest.mark.django_db
def test_exception_when_saving_with_no_items(proforma: Proforma):
    proformer = ProformaService(proforma)
    proformer.drop_items()

    with pytest.raises(InvoiceServiceException):
        proformer.save_invoice()


@pytest.mark.django_db
def test_missing_exchange_rate(proformer: ProformaService):
    with pytest.raises(InvoiceServiceException):
        proformer.set_exchange_rate("1990-01-01", currency_code="EUR")


@pytest.mark.django_db
def test_correct_exchange_rate_set(proformer: ProformaService, exchange_rate: ExchangeRate):
    proformer.set_exchange_rate(
        exchange_date=exchange_rate.exchange_date,
        currency_code=exchange_rate.currency_code,
    )

    assert exchange_rate.exchange_rate == proformer.invoice.exchange_rate


@pytest.mark.django_db
def test_if_proforma_correctly_saved(proforma: Proforma):
    proformer = ProformaService(proforma)

    # assert that contacts have been properly set
    assert proforma.proforma_billing_address.id == ProformaBillingAddress.objects.latest("id").id
    assert proformer.billing.id == ProformaBillingAddress.objects.latest("id").id
    assert proforma.proforma_delivery_address.id == ProformaDeliveryAddress.objects.latest("id").id
    assert proformer.delivery.id == ProformaDeliveryAddress.objects.latest("id").id

    # proforma should be unpaid when first created
    assert proforma.paid is False

    # check for proper incrementation of the sequence
    seq = Sequence.objects.get(name="test_quote")
    assert int(INITIAL_SEQUENCE_NUMBERS[1][1]) == seq.last - 1

    # check if the proforma has 2 items as according to test conf
    assert proforma.proforma_items.count() == 2

    # check if total vat amount was properly calculated
    total_vat = round(Decimal(48) * Decimal(0.19), 2)
    assert proforma.total_vat() == total_vat

    # check if netto amount was properly calculated
    total_nettto = Decimal(96)
    assert proforma.total_netto() == total_nettto

    # check if gross amount was properly calculated
    total_gross = total_nettto + total_vat
    assert proforma.total_gross() == total_gross


@pytest.mark.django_db
def test_recalcualtion_of_totals_when_adding_item(proforma: Proforma):
    proformer = ProformaService(proforma)
    old_total_vat = proformer.invoice.total_vat()
    old_total_netto = proformer.invoice.total_netto()

    # check if totals are properly recalculated when adding a new item
    proformer.add_item(20, 19.00, 1, "b", "Test item #3")
    proformer.save_invoice()

    proforma = proformer.invoice

    assert len(proformer.invoice_items) == 3
    assert proforma.proforma_items.count() == 3

    # check new total_vat
    new_total_vat = old_total_vat + round(Decimal(20) * Decimal(0.19), 2)
    assert proforma.total_vat() == new_total_vat

    # check new total_netto
    new_total_netto = old_total_netto + Decimal(20)
    assert proforma.total_netto() == new_total_netto

    # check new total_gross
    new_total_gross = new_total_netto + new_total_vat
    assert proforma.total_gross() == new_total_gross


@pytest.mark.django_db
def test_mark_as_paid(proforma: Proforma):
    proformer = ProformaService(proforma)
    proformer.mark_paid()

    assert proformer.invoice.paid is True


@pytest.mark.django_db
def test_no_updates_if_paid(proforma: Proforma):
    proformer = ProformaService(proforma)
    proformer.mark_paid()

    with pytest.raises(InvoiceServiceException):
        proformer.add_item(20, 19.00, 1, "b", "test")


@pytest.mark.django_db
def test_add_comment(proforma: Proforma, user: User):
    proformer = ProformaService(proforma)
    proformer.add_user_comment(user.pk, "test comment")

    comments = proforma.proforma_comment.all()

    assert len(comments) == 1
    assert comments[0].text == "test comment"
