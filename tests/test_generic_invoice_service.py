from decimal import Decimal

import pytest
from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from sequences.models import Sequence

from agile_accounting.models import (
    ExchangeRate,
    Invoice,
    InvoiceItem,
    InvoiceBillingAddress,
    InvoiceDeliveryAddress,
    InvoicingEntity,
    MeasureUnit,
)
from agile_accounting.services import Contact, InvoiceServiceException, InvoiceServiceGeneric
from tests.conftest import INITIAL_SEQUENCE_NUMBERS


@pytest.mark.django_db
def test_invoice_invalid_reconstitution():
    with pytest.raises(InvoiceServiceException):
        InvoiceServiceGeneric(9999999)


@pytest.mark.django_db
def test_correct_reconstitution(invoice: Invoice):
    invoicer = InvoiceServiceGeneric(invoice)

    assert invoicer.invoice == invoice
    assert len(invoicer.invoice_items) == invoice.items.count()
    assert invoicer.billing == invoice.billing_address
    assert invoicer.delivery == invoice.delivery_address


@pytest.mark.django_db
def test_missing_billing(generic_invoicer: InvoiceServiceGeneric):
    with pytest.raises(InvoiceServiceException) as e:
        generic_invoicer.save_invoice()
        assert e == "Must provide a billing contact"


@pytest.mark.django_db
def test_invalid_billing(generic_invoicer: InvoiceServiceGeneric, contact: Contact):
    with pytest.raises(InvoiceServiceException) as e:
        generic_invoicer.set_contact("unknown", contact)
        assert e == "Target must be one of billing|delivery"


@pytest.mark.django_db
def test_missing_invoicing_entity(generic_invoicer: InvoiceServiceGeneric, contact: Contact):
    generic_invoicer.set_contact("billing", contact)

    with pytest.raises(InvoiceServiceException) as e:
        generic_invoicer.save_invoice()
        assert e == "Must provide a billing contact"


@pytest.mark.django_db
def test_invalid_invoicing_entity(generic_invoicer: InvoiceServiceGeneric):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_invoicing_entity(123123123123)
        generic_invoicer.set_invoicing_entity("wrong code")


@pytest.mark.django_db
def test_invalid_currency(generic_invoicer: InvoiceServiceGeneric):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_currency("wrong currency")


@pytest.mark.django_db
def test_invalid_delivery_type(generic_invoicer: InvoiceServiceGeneric):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_delivery_type("wrong delivery type")


@pytest.mark.django_db
def test_missing_service_type(
    generic_invoicer: InvoiceServiceGeneric, contact: Contact, invoicing_entity: InvoicingEntity
):
    generic_invoicer.set_contact("billing", contact)
    generic_invoicer.set_invoicing_entity(invoicing_entity)

    with pytest.raises(InvoiceServiceException):
        generic_invoicer.save_invoice()


@pytest.mark.django_db
def test_invalid_service_type(generic_invoicer: InvoiceServiceGeneric):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_service_type("wrong service type")


@pytest.mark.django_db
def test_invalid_payment_type(generic_invoicer: InvoiceServiceGeneric):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_payment_type("wrong payment type")


@pytest.mark.django_db
def test_invalid_unit_price(generic_invoicer: InvoiceServiceGeneric):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.add_item("c", 19.00, 2, "b", "test")


@pytest.mark.django_db
def test_invalid_vat_rate(generic_invoicer: InvoiceServiceGeneric):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.add_item(12, 100.00, 2, "b", "test")


@pytest.mark.django_db
def test_exception_when_saving_with_no_items(invoice: Invoice):
    generic_invoicer = InvoiceServiceGeneric(invoice)
    generic_invoicer.drop_items()

    with pytest.raises(InvoiceServiceException):
        generic_invoicer.save_invoice()


@pytest.mark.django_db
def test_missing_exchange_rate(generic_invoicer: InvoiceServiceGeneric):
    with pytest.raises(InvoiceServiceException):
        generic_invoicer.set_exchange_rate("1990-01-01", currency_code="EUR")


@pytest.mark.django_db
def test_correct_exchange_rate_set(generic_invoicer: InvoiceServiceGeneric, exchange_rate: ExchangeRate):
    generic_invoicer.set_exchange_rate(
        exchange_date=exchange_rate.exchange_date,
        currency_code=exchange_rate.currency_code,
    )

    assert exchange_rate.exchange_rate == generic_invoicer.invoice.exchange_rate


@pytest.mark.django_db
def test_sequence_rollback_in_case_of_error(mocker, invoice: Invoice):
    def save():
        raise IntegrityError()

    mocker.patch.object(invoice, "save", new=save)
    old_invoice_number = invoice.invoice_number
    invoice.invoice_number = None
    generic_invoicer = InvoiceServiceGeneric(invoice)

    try:
        generic_invoicer.save_invoice()
    except Exception:
        pass

    seq = Sequence.objects.get(name="test_invoice")

    assert old_invoice_number == seq.last


@pytest.mark.django_db
def test_if_invoice_correctly_saved(invoice: Invoice):
    generic_invoicer = InvoiceServiceGeneric(invoice)

    # assert that contacts have been properly set
    assert invoice.billing_address.id == InvoiceBillingAddress.objects.latest("id").id
    assert generic_invoicer.billing.id == InvoiceBillingAddress.objects.latest("id").id
    assert invoice.delivery_address.id == InvoiceDeliveryAddress.objects.latest("id").id
    assert generic_invoicer.delivery.id == InvoiceDeliveryAddress.objects.latest("id").id

    # invoice should be unpaid when first created
    assert invoice.paid is False

    # check for proper incrementation of the sequence
    seq = Sequence.objects.get(name="test_invoice")
    assert int(INITIAL_SEQUENCE_NUMBERS[0][1]) == seq.last - 1

    # check if the invoice has 2 items as according to test conf
    assert invoice.items.count() == 2

    # check if total vat amount was properly calculated
    total_vat = round(Decimal(48) * Decimal(0.19), 2)
    assert invoice.total_vat() == total_vat

    # check if netto amount was properly calculated
    total_nettto = Decimal(96)
    assert invoice.total_netto() == total_nettto

    # check if gross amount was properly calculated
    total_gross = total_nettto + total_vat
    assert invoice.total_gross() == total_gross


@pytest.mark.django_db
def test_recalcualtion_of_totals_when_adding_item(invoice: Invoice):
    generic_invoicer = InvoiceServiceGeneric(invoice)
    old_total_vat = generic_invoicer.invoice.total_vat()
    old_total_netto = generic_invoicer.invoice.total_netto()

    # check if totals are properly recalculated when adding a new item
    generic_invoicer.add_item(20, 19.00, 1, "b", "Test item #3")
    generic_invoicer.save_invoice()

    invoice = generic_invoicer.invoice

    assert len(generic_invoicer.invoice_items) == 3
    assert invoice.items.count() == 3

    # check new total_vat
    new_total_vat = old_total_vat + round(Decimal(20) * Decimal(0.19), 2)
    assert invoice.total_vat() == new_total_vat

    # check new total_netto
    new_total_netto = old_total_netto + Decimal(20)
    assert invoice.total_netto() == new_total_netto

    # check new total_gross
    new_total_gross = new_total_netto + new_total_vat
    assert invoice.total_gross() == new_total_gross


@pytest.mark.django_db
def test_mark_as_paid(invoice: Invoice):
    generic_invoicer = InvoiceServiceGeneric(invoice)
    generic_invoicer.mark_paid()

    assert generic_invoicer.invoice.paid is True


@pytest.mark.django_db
def test_no_updates_if_paid(invoice: Invoice):
    generic_invoicer = InvoiceServiceGeneric(invoice)
    generic_invoicer.mark_paid()

    with pytest.raises(InvoiceServiceException):
        generic_invoicer.add_item(20, 19.00, 1, "b", "test")


@pytest.mark.django_db
def test_add_comment(invoice: Invoice, user: User):
    generic_invoicer = InvoiceServiceGeneric(invoice)
    generic_invoicer.add_user_comment(user.pk, "test comment")

    comments = invoice.comment.all()

    assert len(comments) == 1
    assert comments[0].text == "test comment"


@pytest.mark.django_db
def test_update_item(invoice: Invoice):
    generic_invoicer = InvoiceServiceGeneric(invoice)
    item_id = generic_invoicer.invoice_items[0].pk

    new_unit_price = "123.12"
    generic_invoicer.update_item(item_id, unit_price=new_unit_price)

    item = InvoiceItem.objects.get(pk=item_id)
    assert item.unit_price == Decimal(new_unit_price)

    new_vat_percent = "0.00"
    generic_invoicer.update_item(item_id, vat_percent=new_vat_percent)

    item = InvoiceItem.objects.get(pk=item_id)
    assert item.vat_percent == Decimal(new_vat_percent)

    new_quantity = 20
    generic_invoicer.update_item(item_id, quantity=new_quantity)

    item = InvoiceItem.objects.get(pk=item_id)
    assert item.quantity == new_quantity

    new_quantity = 20
    generic_invoicer.update_item(item_id, quantity=new_quantity)

    item = InvoiceItem.objects.get(pk=item_id)
    assert item.quantity == Decimal(new_quantity)

    new_measure_unit = MeasureUnit.objects.get(code="y")
    generic_invoicer.update_item(item_id, measure_unit=new_measure_unit)

    item = InvoiceItem.objects.get(pk=item_id)
    assert item.measure_unit == new_measure_unit

    new_description = "New test item description"
    generic_invoicer.update_item(item_id, description=new_description)

    item = InvoiceItem.objects.get(pk=item_id)
    assert item.description == new_description

    new_vat_included = True
    generic_invoicer.update_item(item_id, vat_included=new_vat_included)

    item = InvoiceItem.objects.get(pk=item_id)
    assert item.vat_included == new_vat_included
