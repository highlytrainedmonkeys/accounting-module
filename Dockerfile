FROM python:3.9-bullseye

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE 1

RUN apt-get update && apt-get install -y git curl netcat xz-utils gnupg build-essential

RUN mkdir /home/src
WORKDIR /home/src

COPY requirements-dev.txt .
COPY requirements.txt .

RUN pip install --upgrade pip && pip install -r requirements-dev.txt

COPY . .