import logging
from decimal import Decimal

from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import post_migrate

logger = logging.getLogger(__name__)


def import_required_data(sender, **kwargs):
    saving_models = [
        (settings.SERVICE_TYPES, sender.get_model("ServiceType")),
        (settings.PAYMENT_TYPES, sender.get_model("PaymentType")),
        (settings.DELIVERY_TYPES, sender.get_model("DeliveryType")),
        (settings.CURRENCIES_TYPES, sender.get_model("CurrencyType")),
        (settings.MEASURE_UNITS, sender.get_model("MeasureUnit")),
    ]

    for codes, model in saving_models:
        for code, name in codes.items():
            model_instance = model(code=code, name=name)
            model_instance.save()


def import_required_vatrates(sender, **kwargs):
    VATRateModel = sender.get_model("VATRate")

    for rate in ["0.00", "19.00"]:
        vatrate = VATRateModel(Decimal(rate))
        vatrate.save()


class Config(AppConfig):
    name = "agile_accounting"
    verbose_name = "Invoice Configuration"

    def ready(self):
        post_migrate.connect(import_required_data, sender=self)
        post_migrate.connect(import_required_vatrates, sender=self)
