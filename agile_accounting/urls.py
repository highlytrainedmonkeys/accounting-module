from django.urls import path
from .views import ProformaPDFView, InvoicePDFView

urlpatterns = [
    path("proforma/<int:pk>/<str:token>", ProformaPDFView.as_view(), name="proforma-download-pdf"),
    path("invoice/<int:pk>/<str:token>", InvoicePDFView.as_view(), name="invoice-download-pdf"),
]
