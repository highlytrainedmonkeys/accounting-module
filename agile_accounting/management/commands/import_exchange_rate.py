import datetime
import logging
from urllib.request import urlopen

import xmltodict
from django.core.management.base import BaseCommand

from agile_accounting.models import ExchangeRate

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Fetches the exchange rate from BNR"
    recent_bnr_xml_url = "http://www.bnr.ro/nbrfxrates.xml"

    def save_exchange_rate(self, publishing_date, exchange_date, exchange_rate):
        for currency_code, rate in exchange_rate.items():
            if currency_code not in ["EUR", "USD"]:
                continue

            obj, created = ExchangeRate.objects.get_or_create(
                exchange_date=exchange_date,
                currency_code=currency_code,
                defaults={
                    "publishing_date": publishing_date,
                    "exchange_rate": rate,
                },
            )

            if created is False:
                logger.debug("Exchange rate already fetched in a previous transaction")

    def handle(self, *args, **options):
        try:
            xml = urlopen(self.recent_bnr_xml_url).read()
            document = xmltodict.parse(xml)
            cube = document["DataSet"]["Body"]["Cube"]["Rate"]
        except Exception as e:
            logger.error("Error fetching xml document from BNR: %s", str(e))
            return

        exchange_rate = {}

        for rate in cube:
            exchange_rate[rate["@currency"].upper()] = rate["#text"]

        logger.debug(
            "Found exchange rate EUR=%.4f USD=%.4f",
            float(exchange_rate["EUR"]),
            float(exchange_rate["USD"]),
        )

        today = datetime.date.today()

        # luni, marti, miercuri si joi totul este ok, punem cursul valabil maine
        if today.weekday() in range(0, 4):

            exchange_date = today + datetime.timedelta(days=1)
            self.save_exchange_rate(today, exchange_date, exchange_rate)
        # vineri punem cursul pentru sambata, duminica si luni
        elif today.weekday() == 4:
            for i in range(1, 4):
                exchange_date = today + datetime.timedelta(days=i)
                self.save_exchange_rate(today, exchange_date, exchange_rate)
        else:
            # in weekend nu facem nimic
            pass

        logger.info("Exchange rates fetched from BNR")
