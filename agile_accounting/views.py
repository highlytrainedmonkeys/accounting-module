import io

from django.http import HttpResponse
from django.template.loader import get_template
from django.views import View
from xhtml2pdf import pisa

from .services import InvoiceService, ProformaService


class ProformaPDFView(View):
    def get(self, request, pk=None):
        inv = ProformaService(int(pk))
        template = get_template("proforma_pdf.html")
        html = template.render(
            {
                "invoice": inv.invoice,
                "seller": inv.invoice.invoicing_entity,
                "buyer": inv.invoice.proforma_billing_address,
                "items": inv.invoice.proforma_items.all(),
            }
        )
        result = io.BytesIO()
        pisa.pisaDocument(io.StringIO(html), dest=result)
        response = HttpResponse(result.getvalue(), content_type="application/pdf")
        response["Content-Disposition"] = 'inline; filename="proforma_%d.pdf"' % 1
        return response


class InvoicePDFView(View):
    def get(self, request, pk=None):
        inv = InvoiceService(int(pk))
        template = get_template("invoice_pdf.html")
        html = template.render(
            {
                "invoice": inv.invoice,
                "seller": inv.invoice.invoicing_entity,
                "buyer": inv.invoice.billing_address,
                "items": inv.invoice.items.all(),
            }
        )
        result = io.BytesIO()
        pisa.pisaDocument(io.StringIO(html), dest=result)
        response = HttpResponse(result.getvalue(), content_type="application/pdf")
        response["Content-Disposition"] = 'inline; filename="proforma_%d.pdf"' % 1
        return response
