import datetime
import uuid
from decimal import Decimal

from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _
from sequences.models import Sequence

from ag_core import models as ag_models


def no_updates_if_paid(f):
    def f_wrapper(self, *args, **kwargs):
        if self.paid is True and self.pk is not None:
            raise ValidationError(_("Payment status of this invoice does not allow such an update"))
        f(self, *args, **kwargs)

    return f_wrapper


def get_proforma_due_date():
    return datetime.datetime.now() + datetime.timedelta(days=settings.PROFORMA_DUE_DATE_DAYS)


def get_invoice_due_date():
    return datetime.datetime.now() + datetime.timedelta(days=settings.INVOICE_DUE_DATE_DAYS)


class CurrencyType(ag_models.CodeNameModel):
    class Meta:
        db_table = "currency_type"
        verbose_name = _("Currency")
        verbose_name_plural = _("Currencies")


class PaymentType(ag_models.CodeNameModel):
    class Meta:
        db_table = "payment_type"
        verbose_name = _("Payment Type")
        verbose_name_plural = _("Payment Types")


class DeliveryType(ag_models.CodeNameModel):
    class Meta:
        db_table = "delivery_type"
        verbose_name = _("Delivery Type")
        verbose_name_plural = _("Delivery Types")


class ServiceType(ag_models.CodeNameModel):
    class Meta:
        db_table = "service"
        verbose_name = _("Service Type")
        verbose_name_plural = _("Service Types")


class MeasureUnit(ag_models.CodeNameModel):
    class Meta:
        db_table = "measure_unit"
        verbose_name = _("Measure Unit")
        verbose_name_plural = _("Measure Units")


class ExchangeRate(models.Model):
    models.AutoField(primary_key=True)

    exchange_date = models.DateField(
        _("Exchange Date"),
    )
    publishing_date = models.DateField(
        _("Publishing Date"),
        help_text=_("The date National Bank published the rate"),
    )
    currency_code = models.CharField(
        _("Currency Code"),
        max_length=3,
    )
    exchange_rate = models.DecimalField(
        _("Exchange Rate"),
        max_digits=8,
        decimal_places=4,
    )

    class Meta:
        db_table = "exchange_rate"
        verbose_name = _("Exchange Rate")
        verbose_name_plural = _("Exchange Rates")
        unique_together = (("exchange_date", "currency_code"),)

    def __str__(self):
        return "{:f} LEI/{:s}  {:%Y-%m-%d}".format(
            self.exchange_rate,
            self.currency_code,
            self.publishing_date,
        )


class VATRate(models.Model):
    vat_percentage = models.DecimalField(
        _("VAT Percentage"),
        primary_key=True,
        max_digits=4,
        decimal_places=2,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100),
        ],
    )

    class Meta:
        db_table = "vatrate"
        verbose_name = _("Vat Rate Percentage")
        verbose_name_plural = _("Vat Rate Percentages")

    def value(self):
        return self.vat_percentage / 100

    def __str__(self):
        return "{} %".format(str(self.vat_percentage))


class InvoicingEntity(ag_models.AddressModel):
    code = models.CharField(
        _("Entity Code"),
        max_length=16,
        unique=True,
    )
    invoice_sequence = models.ForeignKey(
        Sequence,
        on_delete=models.PROTECT,
        related_name="invoice_entity",
        verbose_name=_("Invoice Sequence Name"),
        default="rotld_invoice",
    )
    invoice_series = models.CharField(
        _("Invoice Series"),
        max_length=16,
        unique=True,
    )
    quote_sequence = models.ForeignKey(
        Sequence,
        on_delete=models.PROTECT,
        related_name="quote_entity",
        verbose_name=_("Quote Sequence Name"),
        default="rotld_quote",
    )

    class Meta:
        db_table = "invoicing_entity"
        verbose_name = _("Invoicing Entity")
        verbose_name_plural = _("Invoicing Entities")

    def __str__(self):
        return "[{0}] {1}".format(self.code, self.name)


class InvoiceModel(ag_models.CommonModel):
    currency = models.ForeignKey(
        CurrencyType,
        on_delete=models.PROTECT,
        default="RON",
    )
    invoicing_entity = models.ForeignKey(
        InvoicingEntity,
        on_delete=models.PROTECT,
        verbose_name=_("Invoicing Entity"),
    )
    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
    )
    delivery_type = models.ForeignKey(
        DeliveryType,
        on_delete=models.PROTECT,
        verbose_name=_("Delivery Method"),
        default="email",
    )
    service_type = models.ForeignKey(
        ServiceType,
        on_delete=models.PROTECT,
        verbose_name=_("Service"),
    )
    exchange_rate = models.DecimalField(
        _("Exchange Rate"),
        decimal_places=4,
        max_digits=8,
        null=True,
        blank=True,
    )
    exchange_date = models.DateField(
        _("Exchange Date"),
        null=True,
        blank=True,
    )

    class Meta:
        abstract = True


class InvoiceItemModel(ag_models.CommonModel):
    description = models.CharField(
        _("Item description"),
        max_length=128,
    )
    unit_price = models.DecimalField(
        _("Unit price"),
        max_digits=12,
        decimal_places=2,
    )
    vat_percent = models.DecimalField(
        _("VAT %"),
        decimal_places=2,
        max_digits=4,
        validators=[
            MinValueValidator(0),
            MaxValueValidator(100),
        ],
    )
    quantity = models.DecimalField(
        _("Quantity"),
        max_digits=12,
        decimal_places=2,
        default=1,
    )

    measure_unit = models.ForeignKey(
        MeasureUnit,
        on_delete=models.PROTECT,
        verbose_name=_("Measure Unit"),
        blank=False,
        null=False,
        default="b",
    )

    class Meta:
        abstract = True

    vat_included = models.BooleanField(
        null=False,
        blank=False,
        default=False,
        db_index=True,
        verbose_name=_("VAT Included?"),
    )

    def total_netto(self):
        if self.vat_included is False:
            total = self.unit_price * self.quantity
        else:
            total = self.total_gross() / (1 + self.vat_percent / 100)
        return total.quantize(Decimal("0.01"))

    def total_vat(self):
        if self.vat_included is False:
            total = self.total_netto() * (self.vat_percent / 100)
        else:
            total = self.total_gross() - self.total_netto()
        return total.quantize(Decimal("0.01"))

    def total_gross(self):
        if self.vat_included is False:
            total = self.total_netto() + self.total_vat()
        else:
            total = self.unit_price * self.quantity
        return total.quantize(Decimal("0.01"))


class Invoice(InvoiceModel):
    paid = models.BooleanField(
        _("Paid?"),
        default=False,
        db_index=True,
        help_text=_("Check this to mark this invoice as paid."),
    )
    delivered = models.BooleanField(
        _("Delivered?"),
        default=False,
        db_index=True,
        help_text=_("Check this to mark this invoice as delivered to customer."),
    )
    invoice_number = models.BigIntegerField(
        _("Invoice Number"),
        unique=True,
    )
    invoice_date = models.DateField(
        _("Invoice Date"),
        db_index=True,
        default=datetime.date.today,
    )
    invoice_due_date = models.DateField(
        blank=False,
        null=False,
        db_index=True,
        default=get_invoice_due_date,
        verbose_name=_("Invoice Due Date"),
    )
    payment_type = models.ForeignKey(
        PaymentType,
        on_delete=models.PROTECT,
        verbose_name=_("Payment Method"),
    )
    storno_id = models.IntegerField(
        _("Storno ID"),
        null=True,
        blank=True,
        default=None,
        unique=True,
    )

    class Meta:
        db_table = "invoice"
        verbose_name = _("Invoice")
        verbose_name_plural = _("Invoices")

    def total_netto(self):
        total = Decimal("0.00")
        for item in self.items.all():
            total = total + item.total_netto()
        return total

    def total_vat(self):
        total = Decimal("0.00")
        for item in self.items.all():
            total = total + item.total_vat()
        return total

    def total_gross(self):
        total = Decimal("0.00")
        for item in self.items.all():
            total = total + item.total_gross()
        return total

    def vat_percent(self):
        return self.items.all()[0].vat_percent

    @no_updates_if_paid
    def add_item(self, item):
        if self.pk is None:
            self.save()
        item.invoice = self

        self.items.add(item, bulk=False)

    @no_updates_if_paid
    def drop_items(self):
        if self.pk is None:
            self.save()

        self.items.clear()

    @no_updates_if_paid
    def add_payment(self, payment):
        if self.pk is None:
            self.save()
        payment.invoice = self
        self.payments.add(payment, bulk=False)
        if self.rest_payment() == Decimal(0.0):
            self.paid = True
            self.save()

    def rest_payment(self):
        rest_payment = self.total_gross()

        for payment in self.payments.all():
            rest_payment = rest_payment - payment.amount
        return rest_payment

    def total_paid(self):
        total = Decimal("0.00")

        for payment in self.payments.all():
            total = total + payment.amount

    def __str__(self):
        return "{0:d} / {1:s}".format(
            self.invoice_number,
            str(self.created.date()),
        )


class InvoiceItem(InvoiceItemModel):
    invoice = models.ForeignKey(
        Invoice,
        on_delete=models.CASCADE,
        related_name="items",
        verbose_name=_("Invoice"),
    )

    class Meta:
        db_table = "invoice_item"
        verbose_name = _("Invoice Item")
        verbose_name_plural = _("Invoice Items")


class InvoicePayment(ag_models.CommonModel):
    invoice = models.ForeignKey(
        Invoice,
        on_delete=models.PROTECT,
        related_name="payments",
        verbose_name=_("Invoice"),
    )
    amount = models.DecimalField(
        _("Amount"),
        max_digits=12,
        decimal_places=2,
        validators=[
            MinValueValidator(0.01),
        ],
        help_text=_("The amount paid"),
    )
    description = models.CharField(
        _("Description"),
        max_length=256,
        null=True,
        blank=True,
    )

    class Meta:
        db_table = "invoice_payment"
        verbose_name = _("Invoice Payment")
        verbose_name_plural = _("Invoice Payments")


class InvoiceComment(ag_models.CommentModel):
    invoice = models.ForeignKey(
        Invoice,
        on_delete=models.CASCADE,
        related_name="comment",
        verbose_name=_("Invoice"),
    )

    class Meta:
        db_table = "invoice_comment"
        verbose_name = _("Invoice Comment")
        verbose_name_plural = _("Invoice Comments")


class InvoiceBillingAddress(ag_models.AddressModel, ag_models.BankDetails):
    invoice = models.OneToOneField(
        Invoice,
        on_delete=models.CASCADE,
        unique=True,
        related_name="billing_address",
        verbose_name=_("Invoice"),
    )

    class Meta:
        db_table = "invoice_billing_address"
        verbose_name = _("Billing Address")
        verbose_name_plural = _("Billing Addresses")


class InvoiceDeliveryAddress(ag_models.AddressModel, ag_models.BankDetails):
    invoice = models.OneToOneField(
        Invoice,
        on_delete=models.CASCADE,
        related_name="delivery_address",
        verbose_name=_("Invoice"),
        blank=True,
        null=True,
    )

    class Meta:
        db_table = "invoice_delivery_address"
        verbose_name = _("Delivery Address")
        verbose_name_plural = _("Delivery Addresses")


class Proforma(InvoiceModel):
    paid = models.BooleanField(
        _("Paid?"),
        default=False,
        help_text=_("Check this to mark this proforma as paid."),
    )
    proforma_number = models.BigIntegerField(
        _("Proforma Number"),
        unique=True,
    )
    proforma_date = models.DateField(
        _("Proforma Date"),
        default=datetime.date.today,
    )
    proforma_due_date = models.DateField(
        blank=False,
        null=False,
        db_index=True,
        default=get_proforma_due_date,
        verbose_name=_("Proforma Due Date"),
    )
    invoice = models.ForeignKey(
        Invoice,
        on_delete=models.SET_NULL,
        related_name="proformas",
        verbose_name=_("Invoice"),
        blank=True,
        null=True,
    )

    class Meta:
        db_table = "proforma"
        verbose_name = _("Proforma")
        verbose_name_plural = _("Proformas")

    def total_netto(self):
        total = Decimal("0.00")

        for item in self.proforma_items.all():
            total = total + item.total_netto()
        return total

    def total_vat(self):
        total = Decimal("0.00")

        for item in self.proforma_items.all():
            total = total + item.total_vat()
        return total

    def total_gross(self):
        total = Decimal("0.00")

        for item in self.proforma_items.all():
            total = total + item.total_gross()
        return total

    def vat_percent(self):
        return self.proforma_items.all()[0].vat_percent

    @no_updates_if_paid
    def add_item(self, item):
        if self.pk is None:
            self.save()
        item.proforma = self

        self.proforma_items.add(item, bulk=False)

    def __str__(self):
        return "{0:d} / {1:s}".format(
            self.proforma_number,
            str(self.created.date()),
        )


class ProformaItem(InvoiceItemModel):
    proforma = models.ForeignKey(
        Proforma,
        on_delete=models.CASCADE,
        related_name="proforma_items",
        verbose_name=_("Proforma"),
    )

    class Meta:
        db_table = "proforma_item"
        verbose_name = _("Proforma Item")
        verbose_name_plural = _("Proforma Items")


class ProformaComment(ag_models.CommentModel):
    proforma = models.ForeignKey(
        Proforma,
        on_delete=models.CASCADE,
        related_name="proforma_comment",
        verbose_name=_("Proforma"),
    )

    class Meta:
        db_table = "proforma_comment"
        verbose_name = _("Proforma Comment")
        verbose_name_plural = _("Proforma Comments")


class ProformaBillingAddress(ag_models.AddressModel, ag_models.BankDetails):
    proforma = models.OneToOneField(
        Proforma,
        on_delete=models.CASCADE,
        unique=True,
        related_name="proforma_billing_address",
        verbose_name=_("Proforma"),
    )

    class Meta:
        db_table = "proforma_billing_address"
        verbose_name = _("Billing Address")
        verbose_name_plural = _("Billing Addresses")


class ProformaDeliveryAddress(ag_models.AddressModel, ag_models.BankDetails):
    proforma = models.OneToOneField(
        Proforma,
        on_delete=models.CASCADE,
        related_name="proforma_delivery_address",
        verbose_name=_("Proforma"),
        blank=True,
        null=True,
    )

    class Meta:
        db_table = "proforma_delivery_address"
        verbose_name = _("Delivery Address")
        verbose_name_plural = _("Delivery Addresses")
