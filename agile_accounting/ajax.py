from django.http import JsonResponse

from .models import ExchangeRate


def xc_rate(request):
    date = request.GET.get("date")
    currency = request.GET.get("cur")

    if not date and not currency:
        return JsonResponse(
            {
                "error": 1,
                "msg": "Must provide date and currency code!",
            }
        )

    ex_rates = ExchangeRate.objects.filter(publishing_date=date, currency_code=currency)

    if len(ex_rates) == 0:
        return JsonResponse(
            {
                "error": 1,
                "msg": "No exchange rates found for specified date and currency code!",
            }
        )

    ex_rate = ex_rates[0]

    return JsonResponse({"error": 0, "data": str(ex_rate)})
