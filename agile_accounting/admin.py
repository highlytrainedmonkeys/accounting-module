from django.contrib import admin

from .models import (
    Invoice,
    InvoiceComment,
    InvoiceBillingAddress,
    InvoiceDeliveryAddress,
    InvoiceItem,
    InvoicePayment,
    Proforma,
    ProformaBillingAddress,
    ProformaComment,
    ProformaDeliveryAddress,
    ProformaItem,
    ExchangeRate,
    VATRate,
    InvoicingEntity,
    ServiceType,
    DeliveryType,
    PaymentType,
    CurrencyType,
)

from ag_core.models import CountryCode, EntityType


class CodeNameTypeAdmin(admin.ModelAdmin):
    list_display = ("code", "name")
    ordering = ["code", "name"]
    search_fields = ["code", "name"]


class InvoicePaymentInline(admin.TabularInline):
    model = InvoicePayment


class InvoiceCommentInline(admin.TabularInline):
    model = InvoiceComment


class InvoiceItemInline(admin.TabularInline):
    model = InvoiceItem


class InvoiceBillingAddressInline(admin.StackedInline):
    model = InvoiceBillingAddress


class InvoiceDeliveryAddressInline(admin.StackedInline):
    model = InvoiceDeliveryAddress


class ProformaCommentInline(admin.TabularInline):
    model = ProformaComment


class ProformaItemInline(admin.TabularInline):
    model = ProformaItem


class ProformaBillingAddressInline(admin.StackedInline):
    model = ProformaBillingAddress


class ProformaDeliveryAddressInline(admin.StackedInline):
    model = ProformaDeliveryAddress


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = [
        "id",
        "invoice_number",
        "paid",
        "delivered",
        "rest_payment",
        "total_netto",
        "total_vat",
        "total_gross",
        "created",
        "invoice_date",
        "service_type",
        "payment_type",
        "delivery_type",
        "currency",
        "modified",
    ]
    ordering = ("-id",)
    list_filter = (
        "payment_type",
        "delivery_type",
        ("invoice_date", admin.DateFieldListFilter),
        ("created", admin.DateFieldListFilter),
        "service_type",
        "paid",
        "delivered",
        "currency",
    )
    search_fields = ["id", "invoice_number"]
    list_editable = []
    inlines = [
        InvoiceItemInline,
        InvoiceBillingAddressInline,
        InvoiceDeliveryAddressInline,
        InvoicePaymentInline,
        InvoiceCommentInline,
    ]


@admin.register(CountryCode)
class CountryCodeAdmin(admin.ModelAdmin):
    list_display = ("name", "code", "is_eu_member")
    ordering = ["name"]
    search_fields = ["name", "code"]
    list_filter = ("is_eu_member",)


@admin.register(ExchangeRate)
class ExchangeRateAdmin(admin.ModelAdmin):
    list_display = ("id", "exchange_date", "publishing_date", "exchange_rate", "currency_code")
    ordering = ["-id", "exchange_date", "publishing_date"]
    list_filter = ("currency_code",)


@admin.register(ServiceType)
class ApplicationTypeAdmin(CodeNameTypeAdmin):
    pass


@admin.register(PaymentType)
class PaymentTypeAdmin(CodeNameTypeAdmin):
    pass


@admin.register(DeliveryType)
class DeliveryTypeAdmin(CodeNameTypeAdmin):
    pass


@admin.register(CurrencyType)
class CurrencyAdmin(CodeNameTypeAdmin):
    pass


@admin.register(EntityType)
class EntityTypeAdmin(CodeNameTypeAdmin):
    pass


@admin.register(VATRate)
class VATRateAdmin(admin.ModelAdmin):
    pass


@admin.register(InvoicingEntity)
class InvoicingEntityAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "code",
        "id1",
        "id2",
        "invoice_series",
        "invoice_sequence",
        "quote_sequence",
    )


@admin.register(Proforma)
class ProformaAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = [
        "id",
        "proforma_number",
        "paid",
        "total_netto",
        "total_vat",
        "total_gross",
        "created",
        "proforma_date",
        "service_type",
        "delivery_type",
        "currency",
        "modified",
    ]
    ordering = ("-id",)
    list_filter = (
        "delivery_type",
        ("proforma_date", admin.DateFieldListFilter),
        ("created", admin.DateFieldListFilter),
        "service_type",
        "paid",
        "currency",
    )
    search_fields = ["id", "proforma_number"]
    list_editable = []
    inlines = [ProformaItemInline, ProformaBillingAddressInline, ProformaDeliveryAddressInline, ProformaCommentInline]
