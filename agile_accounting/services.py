# -*- coding: utf-8 -*-
"""A module for handling invoices and pro forms.

This module provides with services that handle invoice and pro forms.
"""
import logging
from enum import Enum
from typing import Union

from ag_core.models import CountryCode, EntityType
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import transaction
from django.db.utils import IntegrityError
from django.utils.translation import gettext as _
from sequences import get_next_value

from .models import (  # noqa
    CurrencyType,
    DeliveryType,
    ExchangeRate,
    Invoice,
    InvoiceBillingAddress,
    InvoiceComment,
    InvoiceDeliveryAddress,
    InvoiceItem,
    InvoicePayment,
    InvoicingEntity,
    MeasureUnit,
    PaymentType,
    Proforma,
    ProformaBillingAddress,
    ProformaComment,
    ProformaDeliveryAddress,
    ProformaItem,
    ServiceType,
    VATRate,
)
from .tools import validate_proformas

logger = logging.getLogger(__name__)


class InvoiceServiceException(Exception):
    pass


class ContactTypes(Enum):
    PRIVATE_PERSON = "p"
    COMPANY = "c"


class Contact:
    """
    Class method for setting the billing or delivery contact. \
    It will create an instance of core.models.AddressModel and attached it to the invoice/proforma.

    Args:
        target (:obj:`str`): One of ['billing','delivery']. Mandatory.
        type (:obj:`str`): Contact type. A code for core.models.EntityType, such as 'p' or 'c'. Mandatory.
        name (:obj:`str`): Name of contact. Mandatory.
        email (:obj:`str`): Contact email. Mandatory.
        id1 (:obj:`str`): CNP for persons, Fiscal Code for companies. Mandatory.
        id2 (:obj:`str`, optional): A second identification token. E.g. registration number for companies. Optional.
        bank_account (:obj:`str`, optional): Bank account. Optional.
        bank_name (:obj:`str`, optional): Bank name. Optional.
        phone (:obj:`str`, optional): Phone number in E164 format. E.g. +40722565194. Optional.
        city (:obj:`str`): Fax number in E164 format. E.g. +40722565194. Mandatory.
        country_code (:obj:`str`): A country code for core.models.CountryCode. Mandatory.
        street1 (:obj:`str`): Address line 1. Mandatory.
        street2 (:obj:`str`, optional): Address line 2. Optional.
        postal_code (:obj:`str`, optional): Postal code. Optional.
    """

    type = ContactTypes.PRIVATE_PERSON.value
    name = None
    email = None
    id1 = None
    id2 = None
    phone = None
    city = None
    country_code = None
    street1 = None
    street2 = None
    postal_code = None
    bank_name = None
    bank_account = None

    def __init__(self, instance=None):
        if instance and isinstance(instance, object):
            self.type = instance.type.code
            self.name = instance.name
            self.email = instance.email
            self.id1 = instance.id1
            self.id2 = instance.id2
            self.phone = instance.phone
            self.city = instance.city
            self.country_code = instance.country_code.code
            self.street1 = instance.street1
            self.street2 = instance.street2
            self.postal_code = instance.postal_code
            self.bank_name = instance.bank_name
            self.bank_account = instance.bank_account


class InvoiceServiceGeneric:
    """
    A class that wraps the django models into a service for handling invoices and proforma

    Args:
        invoice: If invoice is not None, the instance will be created based on an existing invoice. \
        Invoice could be an invoice or proforma id, an instance of invoices.models.Invoice or \
        an instance of invoices.models.Proforma
        invoice_class: The class implementing the invoice or proforma service. It defaults\
        to invoices.models.Invoice.

    Returns:
        Instance of invoices.services.InvoiceService or invoices.models.Proforma.
    """

    def __init__(
        self,
        invoice: Union[int, Invoice, Proforma] = None,
        invoice_class: Union[Invoice, Proforma] = Invoice,
    ):
        self.error_exception = None
        self.invoice = invoice_class()
        self.invoice_items = []
        self.billing = None
        self.delivery = None

        if isinstance(invoice, int) is True:
            try:
                self.invoice = invoice_class.objects.get(pk=invoice)
            except ObjectDoesNotExist:
                err = "%s with id %d not found" % (
                    invoice_class.__name__,
                    invoice,
                )
                logger.error(err)
                raise InvoiceServiceException(err)
        elif isinstance(invoice, invoice_class):
            self.invoice = invoice

            if self.invoice.__class__.__name__ == "Proforma":
                self.billing = invoice.proforma_billing_address
                self.delivery = invoice.proforma_delivery_address
                self.invoice_items = [i for i in invoice.proforma_items.all()]
            else:
                self.billing = invoice.billing_address
                self.delivery = invoice.delivery_address
                self.invoice_items = [i for i in invoice.items.all()]

    def no_updates_if_paid(self):
        if self.invoice.paid is True and self.invoice.pk is not None:
            raise InvoiceServiceException(_("Payment status of this invoice does not allow such an update"))

    def set_contact(self, target: str, contact: Contact):
        """
        Class method for setting the billing or delivery contact. \
        It will create an instance of core.models.AddressModel and attached it to the invoice/proforma.

        Args:
            target (:obj:`str`): One of ['billing','delivery']. Mandatory.
            contact (:obj:`Contact`): Contact object. Mandatory.
        Raises:
            InvoiceServiceException
        """
        self.no_updates_if_paid()

        if target not in ["billing", "delivery"]:
            raise InvoiceServiceException("Target must be one of billing|delivery")

        try:
            type = EntityType.objects.get(code=contact.type)
        except ObjectDoesNotExist:
            raise InvoiceServiceException("Entity Type not available")

        try:
            country_code = CountryCode.objects.get(code=contact.country_code)
        except ObjectDoesNotExist:
            raise InvoiceServiceException("Country not available")

        address_class_name = self.invoice.__class__.__name__ + target.capitalize() + "Address"
        ContactClass = globals()[address_class_name]

        if self.invoice.pk is None:
            contact = ContactClass(
                name=contact.name,
                type=type,
                email=contact.email,
                id1=contact.id1,
                id2=contact.id2,
                bank_account=contact.bank_account,
                bank_name=contact.bank_name,
                phone=contact.phone,
                city=contact.city,
                country_code=country_code,
                street1=contact.street1,
                street2=contact.street2,
                postal_code=contact.postal_code,
            )
        else:
            if self.invoice.__class__.__name__ == "Proforma":
                address_field_name = "proforma_" + target + "_address"
            else:
                address_field_name = target + "_address"

            contact = getattr(self.invoice, address_field_name)
            contact.type = type
            contact.country_code = country_code
            contact.name = contact.name
            contact.email = contact.email
            contact.id1 = contact.id1
            contact.id2 = contact.id2
            contact.phone = contact.phone
            contact.city = contact.city
            contact.street1 = contact.street1
            contact.street2 = contact.street2
            contact.postal_code = contact.postal_code
            contact.bank_account = contact.bank_account
            contact.bank_name = contact.bank_name

        try:
            contact.full_clean(exclude=[self.invoice.__class__.__name__.lower()])
        except ValidationError as e:
            self.error_exception = e
            logger.error("Validation error when saving setting contact: {}".format(e))
            raise InvoiceServiceException("Invalid data")

        setattr(self, target, contact)

    def set_currency(self, currency_code: str):
        """
        Class method for setting the currency

        Args:
            currency_code (:obj:`str`): Currency code. E.g. EUR, RON, SUD.
        """
        self.no_updates_if_paid()

        try:
            self.invoice.currency = CurrencyType.objects.get(code=currency_code)
        except ObjectDoesNotExist:
            raise InvoiceServiceException("Currency not available")

    def set_exchange_rate(
        self,
        exchange_date: str,
        exchange_rate: ExchangeRate = None,
        currency_code: str = None,
    ):
        """
        Class method for setting the exchange rate

        Args:
            exchange_date (str): The exchange date in YYYY-MM-DD format.
            exchange_rate (core.models.ExchangeRate): Optional. If not provided,\
            the exchange rate will be automatically retrieved based on exchange_date and currency_code
            currency_code (str): Currency code.

        """
        self.no_updates_if_paid()

        if currency_code is None:
            currency_code = self.invoice.currency.code
        else:
            try:
                currency_code = CurrencyType.objects.get(code=currency_code).code
            except ObjectDoesNotExist:
                raise InvoiceServiceException("Currency not available")

        if exchange_rate is None:
            try:
                rate = ExchangeRate.objects.get(
                    exchange_date=exchange_date,
                    currency_code=currency_code,
                )
                exchange_rate = rate.exchange_rate

            except ObjectDoesNotExist:
                raise InvoiceServiceException("No exchange rate available for this date and currency_code")

        self.invoice.exchange_rate = exchange_rate
        self.invoice.exchange_date = exchange_date

    def set_invoicing_entity(self, invoicing_entity: InvoicingEntity):
        """
        Class method for setting the invoicing entity

        Args:
            invoicing_entity (:obj:`int` , core.models.InvoicingEntity): The id or the instance of core.models.InvoicingEntity.
        """
        self.no_updates_if_paid()

        if isinstance(invoicing_entity, int) is True:
            try:
                self.invoice.invoicing_entity = InvoicingEntity.objects.get(pk=invoicing_entity)
            except ObjectDoesNotExist:
                logger.error("InvoicingEntity with id %d not found", invoicing_entity)
                raise InvoiceServiceException("InvoicingEntity with id %d not found", invoicing_entity)
        elif isinstance(invoicing_entity, str):
            try:
                self.invoice.invoicing_entity = InvoicingEntity.objects.get(code=invoicing_entity)
            except ObjectDoesNotExist:
                logger.error("InvoicingEntity with code %s not found", invoicing_entity)
                raise InvoiceServiceException("InvoicingEntity with code %s not found", invoicing_entity)
        elif isinstance(invoicing_entity, InvoicingEntity):
            self.invoice.invoicing_entity = invoicing_entity

    def set_delivery_type(self, delivery_type_code: str):
        """
        Class method for setting the delivery type

        Args:
            delivery_type_code (str): Delivery type code. E.g. email, posta, onhand.
        """
        try:
            self.invoice.delivery_type = DeliveryType.objects.get(code=delivery_type_code)
        except ObjectDoesNotExist:
            raise InvoiceServiceException("Delivery type not available")

    def set_service_type(self, service_type_code: str):
        """
        Class method for setting the service type

        Args:
            service_type_code (str): Service type code.
        """
        try:
            self.invoice.service_type = ServiceType.objects.get(code=service_type_code)
        except ObjectDoesNotExist:
            raise InvoiceServiceException("Service type not available")

    def set_payment_type(self, payment_type_code: str):
        """
        Class method for setting the payment type

        Args:
            payment_type_code (str): Payment type code.
        """
        try:
            self.invoice.payment_type = PaymentType.objects.get(code=payment_type_code)
        except ObjectDoesNotExist:
            raise InvoiceServiceException("Payment type not available")

    def drop_items(self):
        """
        Class method for deleting all invoice items/products
        """
        self.no_updates_if_paid()

        if self.invoice.__class__.__name__ == "Invoice":
            self.invoice.items.all().delete()
        else:
            self.invoice.proforma_items.all().delete()

        self.invoice_items = []

    def add_item(
        self,
        unit_price: float,
        vat_percent: float,
        quantity: int,
        measure_unit: str,
        description: str,
        vat_included: bool = False,
        force_item: bool = False,
    ):
        """
        Class method for adding an invoice item/product

        Args:
            unit_price (float): Unit price.
            vat_percent (float): The vat percentage, between 0 and 100.
            quantity (int): Quantity.
            measure_unit (string): The measure unit code
            description (str): Item description.
            vat_included (bool): If the item price contains VAT
            force_item (bool): Overrides no_updates_if_paid check
        """
        if force_item is False:
            self.no_updates_if_paid()

        unit_price = str(unit_price)
        vat_percent = str(vat_percent)
        quantity = str(quantity)

        try:
            VATRate.objects.get(vat_percentage=vat_percent)
        except ObjectDoesNotExist:
            raise InvoiceServiceException(_("This VAT percentage is not available"))

        try:
            mu = MeasureUnit.objects.get(code=measure_unit)
        except ObjectDoesNotExist:
            raise InvoiceServiceException(_("This Measure Unit is not available"))

        item_class_name = str(self.invoice.__class__.__name__) + "Item"
        ItemClass = globals()[item_class_name]
        item = ItemClass(
            unit_price=unit_price,
            vat_percent=vat_percent,
            quantity=quantity,
            measure_unit=mu,
            description=description,
            vat_included=vat_included,
        )

        try:
            item.full_clean(exclude=[self.invoice.__class__.__name__.lower()])
        except ValidationError as e:
            self.error_exception = _("Validation error when saving invoice: {}").format(e)
            logger.error(self.error_exception)
            raise InvoiceServiceException(_("Invalid data"))

        self.invoice_items.append(item)

    def update_item(
        self,
        pk: int,
        unit_price: str = None,
        vat_percent: float = None,
        quantity: int = None,
        measure_unit: str = None,
        description: str = None,
        vat_included: bool = None,
    ):
        self.no_updates_if_paid()

        item_class_name = str(self.invoice.__class__.__name__) + "Item"
        ItemClass = globals()[item_class_name]

        try:
            item = ItemClass.objects.get(pk=pk)
        except ItemClass.ObjectDoesNotExist:
            raise InvoiceServiceException("Could not find item with id: %s" % pk)

        if unit_price:
            item.unit_price = unit_price

        if vat_percent:
            item.vat_percent = vat_percent

        if quantity:
            item.quantity = quantity

        if measure_unit:
            item.measure_unit = measure_unit

        if description:
            item.description = description

        if vat_included is not None:
            item.vat_included = vat_included

        item.save()

    def update_items(self, pk: int, items: list):
        self.no_updates_if_paid()

        try:
            with transaction.atomic():
                self.drop_items()

                for item in items:
                    self.add_item(
                        item["unit_price"],
                        item["vat_percent"],
                        item["quantity"],
                        item["measure_unit"],
                        item["description"],
                    )
        except Exception:
            raise InvoiceServiceException("Invalid items data")

    def update_vat_rate(self, vat_percent):
        self.no_updates_if_paid()

        try:
            VATRate.objects.get(vat_percentage=vat_percent)
        except ObjectDoesNotExist:
            raise InvoiceServiceException(_("This VAT percentage is not available"))

        try:
            with transaction.atomic():
                for item in self.invoice_items:
                    item.vat_percent = vat_percent
                    item.save()
        except Exception:
            raise InvoiceServiceException(_("Could not update items VAT rates"))

    def mark_paid(self):
        """
        Class method for setting the invoice payment status as paid
        """
        self.invoice.paid = True

    def set_date(self, date):
        """
        Class method for setting the invocie date

        Args:
            date (str): invocie date of YYYY-MM-DD format.
        """
        class_name = self.invoice.__class__.__name__.lower()
        setattr(self.invoice, class_name + "_date", date)

    def set_due_date(self, date):
        """
        Class method for setting the invocie due date

        Args:
            date (str): invocie due date of YYYY-MM-DD format.
        """
        class_name = self.invoice.__class__.__name__.lower()
        setattr(self.invoice, class_name + "_due_date", date)

    def add_user_comment(self, user_id: int, text: str):
        """
        Class method for adding a comment to the invoice/proforma
        """
        try:
            user = User.objects.get(pk=user_id)
        except ObjectDoesNotExist:
            raise InvoiceServiceException(_("This User is not available"))

        class_name = str(self.invoice.__class__.__name__) + "Comment"
        CommentClass = globals()[class_name]
        comment = CommentClass(user=user, text=text)

        setattr(comment, self.invoice.__class__.__name__.lower(), self.invoice)
        comment.save()

    def save_invoice(self):
        """
        Method for committing the invoice to database, along with the billing and delivery data.

        """
        if self.billing is None:
            raise InvoiceServiceException("Must provide a billing contact")

        try:
            getattr(self.invoice, "invoicing_entity")
        except Exception:
            raise InvoiceServiceException("Must provide an invoicing entity")

        if self.invoice.__class__ == Invoice:
            number_type = "invoice_number"
            sequence = getattr(self.invoice.invoicing_entity, "invoice_sequence")
            invoice_number = getattr(self.invoice, number_type)
        elif self.invoice.__class__ == Proforma:
            number_type = "proforma_number"
            sequence = getattr(self.invoice.invoicing_entity, "quote_sequence")
            invoice_number = getattr(self.invoice, number_type)

        if len(self.invoice_items) == 0:
            raise InvoiceServiceException("No items")

        try:
            with transaction.atomic():
                if invoice_number is None:
                    invoice_number = get_next_value(sequence.name)
                    setattr(self.invoice, number_type, invoice_number)

                try:
                    self.invoice.full_clean()
                except ValidationError as e:
                    self.error_exception = e
                    logger.error("Validation error when saving invoice: {}".format(e))
                    raise InvoiceServiceException("Invalid data")

                self.invoice.save()

                if self.invoice.__class__ == Invoice:
                    self.invoice.items.set(self.invoice_items, bulk=False)
                    self.billing.invoice = self.invoice

                    if self.delivery is not None:
                        self.delivery.invoice = self.invoice
                elif self.invoice.__class__ == Proforma:
                    self.invoice.proforma_items.set(self.invoice_items, bulk=False)
                    self.billing.proforma = self.invoice

                    if self.delivery is not None:
                        self.delivery.proforma = self.invoice

                self.billing.save()

                if self.delivery is not None:
                    self.delivery.save()

                self.invoice.save()

        except IntegrityError as e:
            logger.error("Integrity error when saving invoice: {}".format(e))
            raise InvoiceServiceException("Integrity error occured when saving invoice")


class InvoiceService(InvoiceServiceGeneric):
    def __init__(self, invoice=None):
        super().__init__(invoice, invoice_class=Invoice)
        self.proformas = list(self.invoice.proformas.all())

    def save_invoice(self):
        super().save_invoice()

        if len(self.proformas) > 0:
            try:
                with transaction.atomic():
                    self.invoice.proformas.set(self.proformas, bulk=False)
                    self.invoice.save()
            except IntegrityError as e:
                logger.error("Integrity error when saving invoice: {}".format(e))
                raise InvoiceServiceException(_("Integrity error when saving invoice"))

    def register_payment(self, amount: float, description: str):
        """
        Class method for registering a payment.

        Args:
            amount (float): Gross amount for payment.
            description (str): Short description of payment.
        """
        payment = InvoicePayment(invoice=self.invoice, amount=amount, description=description)

        try:
            payment.full_clean()
        except ValidationError as e:
            self.error_exception = e
            logger.error("Validation error when registering payment for invoice {0}: {1}".format(self.invoice.pk, e))
            raise InvoiceServiceException("Invalid data")

        self.invoice.add_payment(payment)

    def generate_from_proforma(
        self,
        proforma_id: str,
        billing_address: Contact = None,
        delivery_address: Contact = None,
        payment: str = "card",
        delivery: str = None,
        invoice_date: str = None,
        exchange_date: str = None,
    ):

        """
        Class method for generating invoice from existing proforma
        """
        proforma = None

        # raise error if no proforma_ids are sent
        if proforma_id == "":
            raise InvoiceServiceException(_("Please provide at least one valid proforma id"))

        try:
            proforma = Proforma.objects.get(pk=proforma_id)
        except Exception:
            self.error_exception = _("No proforma found with id {}!").format(proforma_id)
            raise InvoiceServiceException(_("No proforma found with id {}!").format(proforma_id))

        if proforma.invoice is not None:
            self.error_exception = _("Proforma with id {} is already invoiced!").format(proforma_id)
            raise InvoiceServiceException(_("Proforma with id {} is already invoiced!").format(proforma_id))

        if proforma.paid:
            self.error_exception = _("Proforma with id {} is already paid!").format(proforma_id)
            raise InvoiceServiceException(_("Proforma with id {} is already paid!").format(proforma_id))

        if billing_address is None:
            self.set_contact("billing", Contact(proforma.proforma_billing_address))
        else:
            self.set_contact("billing", billing_address)

        if delivery_address is None:
            self.set_contact("delivery", Contact(proforma.proforma_delivery_address))
        else:
            self.set_contact("delivery", delivery_address)

        for prod in proforma.proforma_items.all():
            self.add_item(
                prod.unit_price,
                prod.vat_percent,
                prod.quantity,
                prod.measure_unit_id,
                prod.description,
            )

        self.set_invoicing_entity(proforma.invoicing_entity)
        self.set_service_type(proforma.service_type.code)

        if delivery is not None:
            self.set_delivery_type(delivery)
        else:
            self.set_delivery_type(proforma.delivery_type.code)

        self.set_currency(proforma.currency.code)
        self.set_payment_type(payment)

        if exchange_date is not None:
            self.set_exchange_rate(exchange_date, currency_code="EUR")
        else:
            if proforma.exchange_date is not None:
                self.set_exchange_rate(proforma.exchange_date, proforma.exchange_rate)

        if invoice_date:
            self.set_date(invoice_date)

        # bind proforma to invoice
        self.proformas.append(proforma)
        self.save_invoice()

        return

    def generate_from_proformas(
        self,
        proforma_ids: str,
        billing_address: Contact = None,
        delivery_address: Contact = None,
        payment: str = "card",
        delivery: str = None,
        invoice_date: str = None,
        exchange_date: str = None,
    ):
        """
        Class method for generating invoice from multiple existing proformas
        """

        proforma_ids = str(proforma_ids).replace(" ", "").split(",")

        if len(proforma_ids) < 2:
            raise InvoiceServiceException(_("Please provide more than one proforma_id for this method!"))

        proformas = []

        for p in proforma_ids:
            try:
                proforma = Proforma.objects.get(pk=p)

                if proforma.invoice is not None:
                    self.error_exception = _("Proforma with id {} is already invoiced!").format(p)
                    raise InvoiceServiceException(_("Proforma with id {} is already invoiced!").format(p))

                if proforma.paid:
                    self.error_exception = _("Proforma with id {} is already paid!").format(p)
                    raise InvoiceServiceException(_("Proforma with id {} is already paid!").format(p))

                proformas.append(proforma)
            except Exception:
                self.error_exception = _("No proforma found with id {}").format(p)
                raise InvoiceServiceException(_("No proforma found with id {}").format(p))

        err = validate_proformas(proformas)

        if err is not None:
            raise InvoiceServiceException(err)

        if billing_address is None:
            self.set_contact("billing", Contact(proformas[0].proforma_billing_address))
        else:
            self.set_contact("billing", billing_address)

        if delivery_address is None:
            self.set_contact("delivery", Contact(proformas[0].proforma_delivery_address))
        else:
            self.set_contact("delivery", delivery_address)

        for p in proformas:
            for prod in p.proforma_items.all():
                self.add_item(prod.unit_price, prod.vat_percent, prod.quantity, prod.measure_unit_id, prod.description)

            # bind proforma to invoice
            self.proformas.append(p)

        self.set_invoicing_entity(proformas[0].invoicing_entity)
        self.set_service_type(proformas[0].service_type.code)

        if delivery is not None:
            self.set_delivery_type(delivery)
        else:
            self.set_delivery_type(proformas[0].delivery_type.code)

        self.set_currency(proformas[0].currency.code)
        self.set_payment_type(payment)

        if exchange_date:
            self.set_exchange_rate(exchange_date, currency_code="EUR")
        else:
            if proforma.exchange_date is not None:
                self.set_exchange_rate(proforma.exchange_date, proforma.exchange_rate)

        if invoice_date:
            self.set_date(invoice_date)

        self.save_invoice()

    def storno(self, invoice_id: int, user_id: int = None, reason: str = None):
        """
        Class method for generating a storno invoice
        """
        try:
            invoice = Invoice.objects.get(pk=invoice_id)
        except Exception:
            self.error_exception = _("No invoice found with id {}!").format(invoice_id)
            raise InvoiceServiceException(_("No invoice found with id {}!").format(invoice_id))

        self.set_contact("billing", Contact(invoice.billing_address))
        self.set_contact("delivery", Contact(invoice.delivery_address))

        for prod in invoice.items.all():
            unit_price = -prod.unit_price
            self.add_item(unit_price, prod.vat_percent, prod.quantity, prod.measure_unit_id, prod.description)

        self.set_invoicing_entity(invoice.invoicing_entity)
        self.set_service_type(invoice.service_type.code)
        self.set_delivery_type(invoice.delivery_type.code)
        self.set_currency(invoice.currency.code)
        self.set_payment_type(invoice.payment_type.code)

        self.invoice.storno_id = invoice_id

        if invoice.exchange_date is not None:
            self.set_exchange_rate(invoice.exchange_date, invoice.exchange_rate)

        amount = 0
        description = ""
        placed_at = ""
        payment_type = ""

        if len(invoice.payments.all()) > 0:
            for payment in invoice.payments.all():
                amount = amount + payment.amount
                description += payment.description + ","
                payment_type = payment.payment_type.code
                placed_at = payment.placed_at

        try:
            with transaction.atomic():
                self.save_invoice()

                if len(invoice.payments.all()) > 0:
                    self.register_payment(-amount, description[:-1], placed_at, payment_type)

                if reason is not None:
                    self.add_user_comment(user_id, reason)

                invoice.storno_id = self.invoice.pk
                invoice.save()

        except IntegrityError as e:
            self.error_exception = _("Integrity error when saving storno invoice: {}").format(e)
            logger.error("Integrity error when saving storno invoice: {}".format(e))
            raise InvoiceServiceException(_("Integrity error when saving storno invoice"))

        return self.invoice


class ProformaService(InvoiceServiceGeneric):
    def __init__(self, invoice=None):
        super().__init__(invoice, invoice_class=Proforma)
