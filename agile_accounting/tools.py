from django.utils.translation import gettext as _


def is_filtered(**kwargs):
    if kwargs.get("service_type", []):
        return True

    if kwargs.get("date_from", []):
        return True

    if kwargs.get("date_to", []):
        return True

    if kwargs.get("currency", []):
        return True

    if kwargs.get("paid", []):
        return True

    if kwargs.get("search[value]", []) and kwargs["search[value]"][0] != "":
        return True

    return False


def validate_proformas(proformas):
    ref_proforma = proformas[0]
    _proformas = proformas[1:]

    for p in _proformas:
        if p.invoicing_entity_id != ref_proforma.invoicing_entity_id:
            return _("Expected invoicing entity {0} but found {1} for proforma with id {2}").format(
                ref_proforma.invoicing_entity_id, p.invoicing_entity_id, p.id
            )

        if p.service_type != ref_proforma.service_type:
            return _("Expected service type {0} but found {1} for proforma with id {2}").format(
                ref_proforma.service_type, p.service_type, p.id
            )

        if p.currency.code != ref_proforma.currency.code:
            return _("Expected currency {0} but got {1} for proforma with id {2}").format(
                ref_proforma.currency.code, p.currency.code, p.id
            )

        if p.proforma_billing_address.name.lower() != ref_proforma.proforma_billing_address.name.lower():
            return _("Expected Name {0} but got {1} for proforma with id {2}").format(
                ref_proforma.proforma_billing_address.name, p.proforma_billing_address.name, p.id
            )

        id1_billing = p.proforma_billing_address.id1.replace(" ", "")
        id1_billing_ref = ref_proforma.proforma_billing_address.id1.replace(" ", "")

        if id1_billing.lower() != id1_billing_ref.lower():
            return _("Expected ID1 {0} but got {1} for proforma with id {2}").format(
                ref_proforma.proforma_billing_address.id1, p.proforma_billing_address.id1, p.id
            )

        id1_delivery = p.proforma_delivery_address.id1.replace(" ", "")
        id1_delivery_ref = ref_proforma.proforma_delivery_address.id1.replace(" ", "")

        if id1_delivery.lower() != id1_delivery_ref.lower():
            return _("Expected ID1 {0} but got {1} for proforma with id {2}").format(
                ref_proforma.proforma_delivery_address.id1, p.proforma_delivery_address.id1, p.id
            )

    return None
