=====
Agile Accounting
=====

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "accounting" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'sequences.apps.SequencesConfig',
        'agile_accounting',
    ]

    and

    INITIAL_SEQUENCE_NUMBERS = [
        ('invoice',150000),
        ('quote', 100000),
    ]

2. Include the accounting URLconf in your project urls.py like this::

    path('accounting/', include('agile_accounting.urls')),

3. Run `python manage.py migrate` to create the accounting models.

4. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a poll (you'll need the Admin app enabled).
