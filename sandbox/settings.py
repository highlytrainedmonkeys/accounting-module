SECRET_KEY = "secret-key"
DEBUG = True
INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "ag_core",
    "agile_accounting",
    "sequences.apps.SequencesConfig",
]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": "db.sqlite3",
    }
}

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

INITIAL_SEQUENCE_NUMBERS = [
    ("invoice", 1),
    ("quote", 1),
]

SERVICE_TYPES = {
    "sales": "Sales",
}

PAYMENT_TYPES = {
    "online": "Online",
    "ramburs": "Ramburs",
}

DELIVERY_TYPES = {
    "email": "Via Email",
    "courier": "Via Courier",
}

CURRENCIES_TYPES = {
    "RON": "RON",
    "EUR": "EUR",
}

MEASURE_UNITS = {
    "b": "buc",
    "l": "L",
    "g": "G",
    "kg": "Kg",
}

PROFORMA_DUE_DATE_DAYS = 15
INVOICE_DUE_DATE_DAYS = 15