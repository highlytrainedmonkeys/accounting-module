Django==3.2.7
django_extensions==3.1.3
django-sequences==2.6
xhtml2pdf==0.2.5
xmltodict==0.12.0
git+https://mastereve@bitbucket.org/highlytrainedmonkeys/ag-core.git